numbers = [2, 3, 9, 4, 5, 9]
print(f"Orginalna lista: {numbers}")

i = 1
while i < len(numbers):
    key = numbers[i] 
    j = i - 1
    while j >= 0 and key < numbers[j]:
        numbers[j + 1] = numbers[j]
        j -= 1
    numbers[j + 1] = key
    i += 1

print(f"Posortowana lista: {numbers}")